<?php
/**
 * Template Name: Emissions
 */

$context = Timber::get_context();
$emissions_liste = array(
'post_type' => 'emissions',
'posts_per_page' => -1,
'orderby' => array(
    'title' => 'ASC'
));

$emissions_horaire = array(
'post_type' => 'emissions',
'posts_per_page' => -1,
'meta_key' => 'heure_de_debut',
'orderby' => 'meta_value',
'order' => 'ASC'
);

$jour = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];

$context['emissions_liste'] = Timber::get_posts( $emissions_liste );
$context['emissions_horaire'] = Timber::get_posts( $emissions_horaire );
$context['liste_jours'] = $jour;
Timber::render( 'pages/emissions.twig', $context );