<?php
/**
 * Timber starter-theme
 * https://github.com/timber/starter-theme
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

/**
 * If you are installing Timber as a Composer dependency in your theme, you'll need this block
 * to load your dependencies and initialize Timber. If you are using Timber via the WordPress.org
 * plug-in, you can safely delete this block.
 */
$composer_autoload = __DIR__ . '/vendor/autoload.php';
if ( file_exists( $composer_autoload ) ) {
	require_once $composer_autoload;
	$timber = new Timber\Timber();
}

/**
 * This ensures that Timber is loaded and available as a PHP class.
 * If not, it gives an error message to help direct developers on where to activate
 */
if ( ! class_exists( 'Timber' ) ) {

	add_action(
		'admin_notices',
		function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		}
	);

	add_filter(
		'template_include',
		function( $template ) {
			return get_stylesheet_directory() . '/static/no-timber.html';
		}
	);
	return;
}

/**
 * Sets the directories (inside your theme) to find .twig files
 */
Timber::$dirname = array( 'templates', 'views' );

/**
 * By default, Timber does NOT autoescape values. Want to enable Twig's autoescape?
 * No prob! Just set this value to true
 */
Timber::$autoescape = false;


/**
 * We're going to configure our theme inside of a subclass of Timber\Site
 * You can move this to its own file and include here via php's include("MySite.php")
 */
class RadioCampus extends Timber\Site {
	/** Add timber support. */
	public function __construct() {
		add_action( 'after_setup_theme', array( $this, 'theme_supports' ) );
		add_filter( 'timber/context', array( $this, 'add_to_context' ) );
		add_filter( 'timber/twig', array( $this, 'add_to_twig' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		parent::__construct();
		/** Custom styles and scripts */
		add_action('wp_enqueue_scripts', [$this, 'enqueue_scripts_and_styles']);
		$this->add_routes();
	}

	/** This is where you can register custom post types. */
	public function register_post_types() {
		/**
		 * Emissions
		 */
		$labels = array(
			// Le nom au pluriel
			'name' => _x('Émissions', 'Post Type General Name'),
			// Le nom au singulier
			'singular_name' => _x('Émission', 'Post Type Singular Name'),
			// Le libellé affiché dans le menu
			'menu_name' => __('Émissions'),
			// Les différents libellés de l'administration
			'all_items'          => __('Toutes les émissions'),
			'view_item'          => __('Voir les émissions'),
			'add_new_item'       => __('Ajouter une nouvelle émission'),
			'add_new'            => __('Ajouter'),
			'edit_item'          => __('Editer l\'émission'),
			'update_item'        => __('Modifier l\'émission'),
			'search_items'       => __('Rechercher une émission'),
			'not_found'          => __('Non trouvée'),
			'not_found_in_trash' => __('Non trouvée dans la corbeille'),
		);

		$args = array(
			'label'       => __('Émissions'),
			'description' => __('Tous sur nos émissions'),
			'menu_icon'   => 'dashicons-format-audio',
			'labels'      => $labels,
			// On définit les options disponibles dans l'éditeur de notre custom post type ( un titre, un auteur...)
			'supports' => array('title', 'editor', 'author', 'thumbnail', 'revisions', 'custom-fields'),
			/* 
			* Différentes options supplémentaires
			*/
			'show_in_rest' => true,
			'public'       => true,
			'publicly_queryable' => true
		);

		register_post_type('emissions', $args);

		/**
		 * Podcasts
		 */
		$labels = array(
			// Le nom au pluriel
			'name' => _x('Podcasts', 'Post Type General Name'),
			// Le nom au singulier
			'singular_name' => _x('Podcast', 'Post Type Singular Name'),
			// Le libellé affiché dans le menu
			'menu_name' => __('Podcasts'),
			// Les différents libellés de l'administration
			'all_items'          => __('Tous les podcasts'),
			'view_item'          => __('Voir les podcasts'),
			'add_new_item'       => __('Ajouter un nouveau podcast'),
			'add_new'            => __('Ajouter'),
			'edit_item'          => __('Editer le podcast'),
			'update_item'        => __('Modifier le podcast'),
			'search_items'       => __('Rechercher un podcast'),
			'not_found'          => __('Non trouvé'),
			'not_found_in_trash' => __('Non trouvé dans la corbeille'),
		);

		$args = array(
			'label'       => __('Podcasts'),
			'description' => __('Tous sur nos podcasts'),
			'menu_icon'   => 'dashicons-controls-volumeon',
			'labels'      => $labels,
			// On définit les options disponibles dans l'éditeur de notre custom post type ( un titre, un auteur...)
			'supports' => array('title', 'custom-fields'),
			/* 
			* Différentes options supplémentaires
			*/
			'show_in_rest' => true,
			'public'       => true,
			'publicly_queryable' => true,
			'has_archive' => true
		);

		register_post_type('podcasts', $args);
	}

	/** This is where you can register custom taxonomies. */
	public function register_taxonomies() {

	}

	public function enqueue_scripts_and_styles() {
		wp_register_style('rherault-styles', get_stylesheet_directory_uri() . '/static/assets/dist/app.css', [], true);
		wp_enqueue_style('rherault-styles');

		wp_register_style('rherault-fontawesome-style', get_stylesheet_directory_uri() . '/static/assets/vendor/fontawesome/css/all.min.css', [], true);
		wp_enqueue_style('rherault-fontawesome');

		wp_register_script('rherault-scripts', get_stylesheet_directory_uri() . '/static/assets/dist/app.js', [], null, true);
		wp_enqueue_script('rherault-scripts');

		wp_register_script('rherault-scripts-runtime', get_stylesheet_directory_uri() . '/static/assets/dist/runtime.js', [], null, true);
		wp_enqueue_script('rherault-scripts-runtime');

		wp_register_script('rherault-fontawesome-scripts', get_stylesheet_directory_uri() . '/static/assets/vendor/fontawesome/js/all.min.js', [], null, true);
		wp_enqueue_script('rherault-fontawesome-scripts');
	}

	/** This is where you add some context
	 *
	 * @param string $context context['this'] Being the Twig's {{ this }}.
	 */
	public function add_to_context( $context ) {
		$context['menu_primary']  = new Timber\Menu('menu-primary');
		$context['menu_footer'] = new Timber\Menu('menu-footer');
		$context['site']  = $this;
		return $context;
	}

	public function theme_supports() {
		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		/*
		 * Enable support for Post Formats.
		 *
		 * See: https://codex.wordpress.org/Post_Formats
		 */
		add_theme_support(
			'post-formats',
			array(
				'aside',
				'image',
				'video',
				'quote',
				'link',
				'gallery',
				'audio',
			)
		);

		add_theme_support( 'menus' );
	}

	/** This is where you can add your own functions to twig.
	 *
	 * @param string $twig get extension.
	 */
	public function add_to_twig( $twig ) {
		$twig->addExtension( new Twig\Extension\StringLoaderExtension() );
		$twig->addFilter( new Twig\TwigFilter( 'myfoo', array( $this, 'myfoo' ) ) );
		return $twig;
	}
}

new RadioCampus();
