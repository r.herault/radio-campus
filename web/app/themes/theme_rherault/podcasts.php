<?php
/**
 * Template Name: Podcasts
 */

$context = Timber::context();
$podcasts = [
    'post_type' => 'podcasts',
    'posts_per_page' => 10,
    'paged' => $paged
];

$context['podcasts'] = new Timber\PostQuery( $podcasts );
Timber::render( 'pages/podcasts.twig', $context );