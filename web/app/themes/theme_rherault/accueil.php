<?php
/**
 * Template Name: Accueil
 */

$context = Timber::context();

$context['news'] = new Timber\PostQuery([
    'post_type' => [
        'post'
    ]
]);

$context['podcasts'] = new Timber\PostQuery([
    'post_type' => 'podcasts',
    'posts_per_page' => 5
]);

$context['emission'] = new Timber\PostQuery([
    'post_type' => 'emissions',
    'name' => 'ecoute-ta-fac'
]);

Timber::render( 'pages/accueil.twig', $context );